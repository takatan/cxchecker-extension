PROJECT := cxchecker-extension

all: src/assets/icons/icon-16.png src/assets/icons/icon-24.png \
	src/assets/icons/icon-48.png src/assets/icons/icon-64.png \
	src/assets/icons/icon-128.png

src/assets/icons/icon-16.png: src/assets/icons/icon-128.png
	convert -resize 16x16 $< $@
src/assets/icons/icon-24.png: src/assets/icons/icon-128.png
	convert -resize 24x24 $< $@
src/assets/icons/icon-48.png: src/assets/icons/icon-128.png
	convert -resize 48x48 $< $@
src/assets/icons/icon-64.png: src/assets/icons/icon-128.png
	convert -resize 64x64 $< $@
# src/assets/icons/icon-128.png: src/assets/icons/icon_128.xcf
#	xcf2png $< > $@

.DEFAULT: all
.PHONY: all dist
