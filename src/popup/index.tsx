// Copyright (c) 2013 TANIGUCHI Takaki
// License: GPL version 3 or later

import * as React from 'react';
import { ChangeEvent } from 'react';
import ReactDOM from 'react-dom/client';
import { tabs, Tabs } from 'webextension-polyfill';

const App: React.FC = () => {
    React.useEffect(() => {
        (async () => {
            await pickup();
        })();
    });
    const [match, setMatch] = React.useState(0);
    const [type, setType] = React.useState<string>(() => localStorage['type'] || 'css');
    const [selector, setSelector] = React.useState<string>(() => localStorage['selector'] || '');

    const onRadioChange = async (e: ChangeEvent<HTMLInputElement>) => {
        const val = e.target.value;
        localStorage['type'] = val;
        setType(val || 'css');
        await pickup();
    };

    const onTextChange = async (e: ChangeEvent<HTMLInputElement>) => {
        const val = e.target.value;
        localStorage['selector'] = val;
        setSelector(val || '');
        await pickup();
    };

    const pickup = async () => {
        let activeTab: Tabs.Tab;
        try {
            const activeTabs = await tabs.query({ active: true, currentWindow: true });
            activeTab = activeTabs[0];
        } catch (error) {
            console.log('[===== Error in sendMessageToActiveTab =====]', error);
            throw `Error in sendMessageToActiveTab`;
        }
        const response = await tabs.sendMessage(activeTab.id as number, { type, query: selector });
        setMatch(response?.length || 0);
    };

    return (
        <div>
            <div>CSS and XPath checker</div>
            <input type="radio" name="rtype" value="css" onChange={onRadioChange} checked={type === 'css'} />
            <label>CSS</label>
            <input type="radio" name="rtype" value="xpath" onChange={onRadioChange} checked={type === 'xpath'} />
            <label>XPath</label> / <span>{match}</span> match(es)
            <input type="text" size={40} value={selector} onChange={onTextChange} />
        </div>
    );
};

const root = ReactDOM.createRoot(document.getElementById('content')!);
root.render(<App />);
