// Copyright (c) 2013-2019 TANIGUCHI Takaki
// License: GPL version 3 or later

import { runtime } from 'webextension-polyfill';

const onRequest = async (request: { type: string; query: string }): Promise<{ length: number }> => {
    cleanCss();
    let result: HTMLElement[] = [];
    if (request.query.length == 0) {
        return { length: 0 };
    }
    try {
        if (request.type === 'css') {
            result = Array.from(document.querySelectorAll<HTMLElement>(request.query));
        } else {
            const nodes = document.evaluate(
                request.query,
                document,
                null,
                XPathResult.ORDERED_NODE_ITERATOR_TYPE,
                null,
            );
            let thisElem = nodes.iterateNext();

            while (thisElem) {
                result.push(thisElem as HTMLElement);
                thisElem = nodes.iterateNext();
            }
        }
    } catch (e) {
        return { length: 0 };
    }
    result.forEach((node) => {
        if (node.style) {
            node.style.background = '#c88';
            node.style.border = 'solid 2px red';
        }
    });
    return { length: result.length };
};

const cleanCss = () => {
    document.querySelectorAll<HTMLElement>('*').forEach((e) => {
        if (e.style) {
            e.style.background = '';
            e.style.border = '';
        }
    });
};

runtime.onMessage.addListener(onRequest);
